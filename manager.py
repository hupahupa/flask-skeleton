from upaty import app, job
from flask.ext.script import Manager

manager = Manager(app)
#manager.add_command('check_expired', job.CheckExpired())
#manager.add_command('notify_expired', job.NotifyExpired())

if __name__ == "__main__":
    manager.run()