# -*- coding: utf-8 -*-
import pytz
import bcrypt
import hashlib, uuid, time
from flask import render_template, request, session
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.login import UserMixin, current_user
from flask.ext.babelex import gettext, lazy_gettext
from sqlalchemy.dialects import postgresql
from sqlalchemy import and_, or_, desc, asc, not_
from sqlalchemy.sql import text
from sqlalchemy.orm.session import Session

from utils.helper import generate_string, utcnow, uni
from dateutil.relativedelta import relativedelta
from flask import url_for, current_app

from sqlalchemy.types import TypeDecorator, DateTime
from datetime import datetime, timedelta
from time import mktime
from urlparse import urlparse

db = SQLAlchemy()

# SQLAlchemy does not support Datetime timezone aware type
# e.g DateTime(tzinfo=pytz.UTC)
# manually create this type.
# Todo: should change all db.DateTime to UTCDateTime
class UTCDateTime(TypeDecorator):
    impl = DateTime
    def process_bind_param(self, value, engine):
        if value is not None:
            if value.tzinfo is None:
                return value.replace(tzinfo=pytz.UTC)
            return value.astimezone(pytz.UTC)

    def process_result_value(self, value, engine):
        if value is not None:
            return value.replace(tzinfo=pytz.UTC)



class User(UserMixin, db.Model):
    __tablename__ = "tbl_user"

    id = db.Column(db.Integer, primary_key=True)
    full_name = db.Column(db.UnicodeText())
    fb_id = db.Column(db.UnicodeText(), unique=True)
    email = db.Column(db.UnicodeText(), unique=True)
    password = db.Column(db.UnicodeText())
    phone_code = db.Column(db.UnicodeText())
    phone_number = db.Column(db.UnicodeText())
    secret_token = db.Column(db.UnicodeText(), unique=True)

    STATUS_PEDING = u"pending"
    STATUS_ACTIVE = u"active"
    status = db.Column(db.Enum([
        STATUS_PEDING, STATUS_ACTIVE
    ], native_enum=False), nullable=False, default=STATUS_PEDING)
    created_at = db.Column(UTCDateTime, default=lambda: pytz.UTC.localize(datetime.utcnow()))



class UserToken(db.Model):
    __tablename__ = "tbl_user_token"

    id = db.Column(db.Integer, primary_key=True)
    created_at = db.Column(UTCDateTime, default=lambda: pytz.UTC.localize(datetime.utcnow()))

    user_id = db.Column(db.Integer, db.ForeignKey('hellohealth_user.id'))
    access_token = db.Column(db.UnicodeText())

    # TODO: Store push notification end points separately
    device_token = db.Column(db.Unicode())
    TYPE_APNS = u"apns"
    TYPE_GCM = u"gcm"
    device_type = db.Column(db.Enum([
        TYPE_APNS, TYPE_GCM
    ], native_enum=False), nullable=True, default=None)

    def setDeviceToken(self, device_type, device_token):
        # TODO: Invalidation instead
        UserToken.query.filter(
            UserToken.device_type == device_type,
            UserToken.device_token == device_token
        ).delete()
        self.device_type = device_type
        self.device_token = device_token

    @classmethod
    def findByToken(cls, token):
        return cls.query.filter(cls.access_token == token).first()

    @classmethod
    def create(cls, user_id):
        token = cls()
        token.user_id = user_id
        # TODO: Generate better access token.
        h = hashlib.sha256(str(uuid.uuid4()))
        h.update(str(user_id))

        epoc = int(time.time())
        h.update(str(epoc))
        token.access_token = uni(h.hexdigest().upper())

        db.session.add(token)
        return token

    @classmethod
    def createNew(cls, user_id):
        # delete all old token
        # one token for one login?
        cls.query.filter(cls.user_id == user_id).delete()
        return cls.create(user_id)