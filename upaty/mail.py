from cStringIO import StringIO

from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
from email.Utils import COMMASPACE, formatdate

from twisted.mail.smtp import sendmail
from twisted.internet.defer import maybeDeferred


def st(s_or_u, coding="utf-8"):
    """Convert a byte string or unicode value into a byte string."""
    if s_or_u is None:
        return None
    try:
        return str(s_or_u)
    except UnicodeEncodeError:
        return unicode(s_or_u).encode(coding)


def send(sender_email, send_to_list, subject, text, attachment=None,
         subtype="plain", reply_to=None, bcc=None, sender_name=None):
    """Example:

        mailDeferred = send(
            "notification@upaty.vn", [
                "ubolonton@gmail.com",
                "duyleminh1402@gmail.com"
            ],
            "Hypertension Alert!", '''
                New updates from <b>your device</b> show hypertension tendency.
            ''',
            subtype="html", sender_name="upaty Notification System"
        )
    """

    assert isinstance(send_to_list, (list, tuple))

    text = st(text.strip(), "utf-8")
    subject = st(subject.strip(), "utf-8")

    textMessage = MIMEText(text, subtype, "utf-8")

    if attachment is not None:
        message = MIMEMultipart()
        message.attach(textMessage)
        message.attach(attachment)
    else:
        message = textMessage

    # TODO: Don't hard-code
    if sender_name is None:
        sender_name = "upaty"

    if sender_name:
        message['From'] = "%s <%s>" % (sender_name, sender_email)
    else:
        message['From'] = sender_email

    if reply_to is not None:
        message['Reply-To'] = reply_to

    message['To'] = COMMASPACE.join(send_to_list)
    message['Date'] = formatdate(localtime=True)
    message['Subject'] = subject

    if bcc is not None:
        send_to_list = send_to_list + bcc

    # NTA FIX: We may want to add Flask's thread-local-vars trickeries
    # to Twisted's threads, rather than hard-coding like this
    from upaty import app
    config = app.config["email"]
    if config.get('debug'):
        print "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
        print message['Subject']
        print message['From'], message['To']
        print text
        print "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
        return maybeDeferred(lambda: None)

    # FIX: main.py should use recursive update logic, not python's
    # broken dict.update
    return sendmail(config.get('server', 'localhost'), sender_email, send_to_list, StringIO(message.as_string()))
