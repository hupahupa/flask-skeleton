from flask import Blueprint
from flask import request, session
from utils.api import api, ApiError, ResponseStatus
from utils import api_validators as v
from flask.ext.babelex import gettext
from upaty.models.models import db
from sqlalchemy import desc


api_node = Blueprint('api', __name__)
"""
#@api_node.route('/api/login', methods=('POST',))
@api(auth = False, validations= {
    'login': [v.required],
    'password': [v.required]
})
def login(data):
    user = User.findByEmailOrPhone(data['login'])

    if user is None or not user.is_valid_password(data['password']):
        raise ApiError(gettext('Invalid login'),
            status=ResponseStatus.UNAUTHENTICATION)

    if user.status != User.STATUS_ACTIVE:
        raise ApiError(gettext('Your account is not active yet.'),
            status=ResponseStatus.UNAUTHENTICATION)

    record = UserToken.createNew(user.id)
    db.session.commit()
    return {
        "success": True,
        "data": {
            "token": record.access_token,
            "profile": user.formatProfile()
        }
    }

api_node.add_url_rule('/api/login', 'login', login, methods=('POST',))

"""