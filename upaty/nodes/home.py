from flask import Blueprint, render_template, redirect, current_app, url_for
from flask import request, session

from utils import helper
from upaty.models.models import db
from flask.ext.login import current_user, login_required
from flask.ext.babelex import gettext
from sqlalchemy import or_
from utils import custom_json as json
from flask.ext.paginate import Pagination


home = Blueprint('home', __name__)


@home.route('/')
def index():
    return render_template('home/index.html')
