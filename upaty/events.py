from twisted.internet.defer import maybeDeferred
from twisted.internet import reactor

from flask.ext.sqlalchemy import models_committed

from upaty.models.models import UserToken
from upaty import mail, apns

def wire(app):
    pass
    # @models_committed.connect_via(app)
    # def sendMessages(sender, changes):
    #     for obj, change in changes:
            # if isinstance(obj, EmailMessage) and change == "insert":
            #     # maybeDeferred is used so that exceptions raised from
            #     # mail.send does not stop other emails from being sent.
            #     # It's probably cleaner to make sure mail.send never
            #     # raises an exception, (always returning a deferred
            #     # that succeeds/fails instead)
            #     message = obj
            #     # TODO: Track status of email
            #     # TODO: Handle errors somehow
            #     reactor.callFromThread(
            #         maybeDeferred, mail.send,
            #         message.sender_email, message.receiver_emails,
            #         message.subject, message.content,
            #         subtype=message.subtype,
            #         sender_name=message.sender_name)
            # elif isinstance(obj, SmsMessage) and change == "insert":
            #     message = obj
            #     reactor.callFromThread(
            #         maybeDeferred, sms.send,
            #         message.phone_number, message.content)
            # elif isinstance(obj, Notification) and change == "insert":
            #     notification = obj
            #     user = notification.user
            #     # TODO: Save and track push notification status
            #     # instead of just sending
            #     for user_token in user.user_tokens.filter(
            #         UserToken.device_token != None
            #     ):
            #         device_token = user_token.device_token
            #         if not device_token:
            #             continue
            #         # TODO: Badge
            #         if user_token.type == UserToken.TYPE_APNS:
            #             reactor.callFromThread(
            #                 maybeDeferred, apns.send,
            #                 [user_token], alert=notification.message)
            #         elif user_token.type == UserToken.TYPE_GCM:
            #             # TODO
            #             pass
