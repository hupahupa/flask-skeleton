import traceback
from twisted.internet import reactor

from flask import Flask, request, render_template, current_app, redirect, flash
from flask_principal import Principal, identity_loaded, RoleNeed, UserNeed
from flask.ext.login import current_user
from flask.ext.babelex import Babel, lazy_gettext
from flask.ext.admin import Admin
from flask.ext.admin.contrib.sqla import ModelView

from upaty.admin.admin import AdminView, AdminIndex
from upaty.models.models import db
from utils.auth import login_manager
from utils.flask_ajaxform import FlaskAjaxForm

from config.main import config
from nodes.home import home
from nodes.api_v1.api import api_node

app = Flask(__name__)
app.debug = config.get("debug", False)
app.config.update(config)

db.init_app(app)
login_manager.init_app(app)

app.register_blueprint(home)
app.register_blueprint(api_node)


form = FlaskAjaxForm(app)
principals = Principal(app)
babel = Babel(app)

# set up Admin backend
# each model Class will have related admin page
admin = Admin(app, index_view=AdminIndex())

@identity_loaded.connect_via(app)
def on_identity_loaded(sender, identity):
    # Set the identity user object
    identity.user = current_user

    # Add the UserNeed to the identity
    if current_user:
        identity.provides.add(UserNeed(current_user.id))

    # Assuming the User model has a list of roles, update the
    # identity with the roles that the user provides
    if current_user.id and current_user.role:
        identity.provides.add(RoleNeed(current_user.role.value))


from utils import helper
app.jinja_env.add_extension('jinja2.ext.loopcontrols')
app.jinja_env.globals.update({
    'enumerate': enumerate,
    'str':str,
    'len':len,
    'getattr': getattr,
    'format_datetime': helper.format_datetime,
    'format_date': helper.format_date,
    'format_time': helper.format_time,
    'current_app': current_app,
    'getLocale': helper.getLocale,
})

@babel.localeselector
def get_locale():
    return helper.getLocale()


@app.errorhandler(403)
def permission_denied(e):
    return render_template('errors/403.html'), 403

@app.errorhandler(404)
def page_not_found(e):
    return render_template('errors/404.html'), 404


# Register custom events
from upaty import events
events.wire(app)


# Email notification on error
if not app.debug:
    from upaty import mail

    @app.errorhandler(Exception)
    def got_error(e):
        stack_trace = traceback.format_exc()
        emails = current_app.config["email"].get("errors", [])

        # TODO: When we grow, we want a centralized event
        # aggregation/filtering/handling system (e.g. to roll up
        # repeated errors)
        reactor.callFromThread(
            mail.send,
            "error-notification@upaty.vn",
            emails,
            u"[Error %s] %s" % (current_app.config.get("env", None), e),
            stack_trace
        )

        flash(unicode(lazy_gettext(
            "There was an unexpected error. We have been notified of the problem and will fix it as soon as possible. Sorry for the inconvenience!"
        )), "error")
        return redirect("/")


# Run a remote REPL if configured
# Connect using credentials specified in "passwd_file" (note that
# passwords are used uncrypted (normally we would leave it blank,
# allowing access only locally). For example, if the port is configured
# to be 7777, and the content of the passwd file is "upaty:",
# then run "ssh upaty@localhost -p 7777"
# >>> from .shell import *
# >>> current_app
# <Flask 'upaty'>
# >>> # Do stuff here
rc_config = app.config.get("remote_console")
if rc_config:
    from utils.twisted_services import remote_console
    reactor.callWhenRunning(lambda: remote_console(rc_config).startService())


from utils.twisted_helpers import run_twisted
import thread
# XXX: This is a temporary hack. This should be put in Flask's startup
# hook (if there's such a thing), which should be able to
# distinguish between running Flask and running twistd.
thread.start_new_thread(run_twisted, ())
