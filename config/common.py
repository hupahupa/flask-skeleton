# -*- coding: utf-8 -*-
import os
CONFIG_DIR = os.path.dirname(os.path.abspath(__file__))
APP_ROOT = CONFIG_DIR.rsplit('/', 1)[0]

AVATAR_DIR = {
    "origin": os.path.join(APP_ROOT, 'upaty/static/uploads/avatar/origin'),
    "thumb": os.path.join(APP_ROOT, 'upaty/static/uploads/avatar/thumb'),
    "small_thumb": os.path.join(APP_ROOT, 'upaty/static/uploads/avatar/small_thumb')
}
AVATAR_SIZES = {
    "thumb": 200,
    "small_thumb": 80
}
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])

config = {
    'DOMAIN': 'upaty.vn',
    'MAX_CONTENT_LENGTH': 2 * 1024 * 1024,
    'AVATAR_DIR': AVATAR_DIR,
    'AVATAR_SIZES': AVATAR_SIZES,
    'ALLOWED_EXTENSIONS': ALLOWED_EXTENSIONS,
    'DATETIME_FORMAT': "%d-%m-%Y %H:%M",
    'DATE_FORMAT': '%d-%m-%Y',
    'TIME_FORMAT': '%H:%M',
    'SECRET_KEY': 'flask-session-insecure-secret-key',
    'SQLALCHEMY_DATABASE_URI': '',
    'SQLALCHEMY_ECHO': False,
    'CSS_SYNC_PORT': 9264,
    'debug': True,
    'email': {
    },
    'apns': {
    },
    'gcm': {},
    'LANGUAGES': (
        ('vi', u'Tiếng Việt'),
        ('en', u'English')
    ),
    'english_domains': ['upaty.com', 'dev.upaty.com']
}
