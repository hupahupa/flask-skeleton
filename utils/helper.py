import string
from random import choice
from PIL import Image
import os, re
from flask import current_app, session, request
from datetime import timedelta, datetime
from dateutil.relativedelta import relativedelta
import pytz

CRASH = object()

def st(u, coding="utf-8"):
    try:
        return str(u)
    except UnicodeEncodeError:
        return unicode(u).encode(coding)


def utcnow():
    return pytz.UTC.localize(datetime.utcnow())


def format_datetime(value, format=None):
    try:
        format = format or current_app.config['DATETIME_FORMAT']

        # convert to client timezone
        value = value + relativedelta(days=current_app.config['tizo_offset'])
        return value.strftime(format)
    except Exception, e:
        return ""

def format_date(value, format=None):
    try:
        format = format or current_app.config['DATE_FORMAT']
        return value.strftime(format)
    except Exception, e:
        return ""

def format_time(value, format=None):
    try:
        format = format or current_app.config['TIME_FORMAT']

        # convert to client timezone
        value = value + relativedelta(days=current_app.config['tizo_offset'])
        return value.strftime(format)
    except Exception, e:
        return ""


def generate_string(length=16):
    return ''.join([choice(string.letters + string.digits) for i in range(length)])


def getThumbSize(size, thumbSize):
    # keep either width or height equals thumbSize
    width = size[0]
    height = size[1]
    ratio = 1.0*width/height
    if width > height:
        thumbWidth = thumbSize
        thumbHeight = int(thumbWidth / ratio)
    else:
        thumbHeight = thumbSize
        thumbWidth = int(thumbHeight * ratio)

    return (thumbWidth, thumbHeight)


def uploadAvatar(stream, filename, cfg):
    dirs = cfg['AVATAR_DIR']
    sizes = cfg['AVATAR_SIZES']

    orig = Image.open(stream)
    img = orig.copy()

    filepath = dirs['origin'] + '/' + filename
    img.save(filepath, orig.format)

    thumb = img.resize(getThumbSize(img.size, sizes["thumb"]), Image.ANTIALIAS)
    filepath = dirs['thumb'] + '/' + filename
    thumb.save(filepath, orig.format)

    small_thumb = img.resize(getThumbSize(img.size, sizes["small_thumb"]), Image.ANTIALIAS)
    filepath = dirs['small_thumb'] + '/' + filename
    small_thumb.save(filepath, orig.format)


def deleteAvatar(filename, cfg):
    dirs = cfg['AVATAR_DIR']
    for key, path in dirs.iteritems():
        path = path + "/" + filename
        if os.path.exists(path) and os.path.isfile(path):
            os.remove(path)

def uni(s, coding="utf-8"):
    if isinstance(s, unicode):
        return s
    return unicode(s, coding)


def getInt(request, key, default=None):
    try:
        return int(request.form.get(key, default))
    except Exception:
        if default is CRASH:
            raise
        return default

def getUnicode(request, key, default=None, crashOnBlank=False):
    try:
        val = request.form.get(key).strip()
        if crashOnBlank:
            assert val
        return uni(val)
    except Exception, e:
        print str(e)
        if default is CRASH:
            raise
        return default

def getBool(request, key):
    val = request.form.get(key, None)
    return val in ('1', 'True', 'true')


email_re = re.compile(
    r"(^[-!#$%&'*+/=?^_`{}|~0-9A-Z]+(\.[-!#$%&'*+/=?^_`{}|~0-9A-Z]+)*"  # dot-atom
    r'|^"([\001-\010\013\014\016-\037!#-\[\]-\177]|\\[\001-011\013\014\016-\177])*"' # quoted-string
    r')@(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+[A-Z]{2,6}\.?$', re.IGNORECASE)


def checkEmail(email):
    if email_re.match(email) is None:
        return False
    return True


def allowedLocales():
    return [k for k, _ in current_app.config['LANGUAGES']]


class UnsupportedLocale(Exception):
    pass

def setLocale(code):
    print "code", code
    locales = allowedLocales()
    if code in locales:
        session["upaty.locale"] = code
    else:
        # TODO: Custom type
        raise UnsupportedLocale(locales)


def getLocale():
    locale = session.get("upaty.locale")
    if locale:
        return locale
    else:
        host = request.headers['Host']
        if host in current_app.config['english_domains']:
            return 'en'

        # Favor first language for now
        return allowedLocales()[0]
        # return request.accept_languages.best_match(allowedLocales())


def format_currency(value):
    try:
        value = int(value)
        value = "{:,}".format(value)
        return value
        #return value.replace(",", ".")
    except Exception, e:
        return 'N/A'

