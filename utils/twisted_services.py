from twisted.conch import manhole_tap

import traceback


def remote_console(config):
    passwd = config.get("passwd_file")
    port = config.get("port")
    if not (port and passwd):
        print("Not enough config for remote console")
        return None
    try:
        from pprint import pprint
        return manhole_tap.makeService({
            "namespace": {
                "pprint": pprint
            },
            "sshPort": "tcp:%s:interface=127.0.0.1" % port,
            "telnetPort": None,
            "passwd": passwd,
        })
    except Exception, e:
        print("Cannot add remote console service")
        print(traceback.format_exc(e))
        return None
