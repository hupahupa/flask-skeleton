from json import *
from json import dumps as _dumps
from datetime import datetime, date

import speaklater


class CustomJSONEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, (date, datetime)):
            return o.isoformat()
        if isinstance(o, speaklater._LazyString):
            return unicode(o)
        return JSONEncoder.default(self, o)


def dumps(obj, cls=CustomJSONEncoder, **kwargs):
    return _dumps(obj, cls=cls, *kwargs)
