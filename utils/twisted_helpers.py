# Run Twisted in another thread
from twisted.internet import reactor
from twisted.internet.defer import Deferred
import thread


def run_twisted():
    return thread.start_new_thread(reactor.run, (), {
        "installSignalHandlers": False
    })


def sleep(seconds):
    d = Deferred()
    reactor.callLater(seconds, lambda: d.callback(None))
    return d
