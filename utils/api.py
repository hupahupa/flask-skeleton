from utils.helper import uni
import json
from flask import Response, request as req
from utils import api_validators as validators
from upaty.models.models import UserToken

class ResponseStatus:
    INVALID_REQUEST = "invalid_request"
    INVALID_METHOD = "invalid_method"
    UNAUTHENTICATION = "unauthentication"
    NOT_FOUND = "not_found"
    VALIDATION = "validation_error"


class ApiError(Exception):
    def __init__(self, message, status = ResponseStatus.VALIDATION, data = None, httpStatus = 400):
        Exception.__init__(self, message)
        self.message = message
        self.status = status
        self.data = data
        self.httpStatus = httpStatus


def doAuth(handler):
    def authed(*args, **kwargs):
        # Todo: handle authenticate by using access token
        data = kwargs.get('data', None)
        access_token = data.get('access_token', None)

        token = UserToken.findByToken(access_token)
        if token is None or token.user is None:
            return {
                "success": False,
                "error": {
                    "code": ResponseStatus.UNAUTHENTICATION,
                    "message": "Authentication Error",
                }
            }

        # XXX: Why are these keyword arguments while they are used as
        # positional arguments?
        kwargs['data'] = data
        kwargs['user'] = token.user
        return handler(*args, **kwargs)
    return authed

# extract data in request header, request body, request params
def extractData(handler):
    def extracted(*args, **kwargs):
        data = {}
        if req.method == "GET":
            data = dict((k, v) for k, v in req.args.iteritems())
        elif req.method == "POST":
            # All data fields are strings in a JSON-encoded body
            try:
                content = req.data
                data = json.loads(content)
            except ValueError:
                return {
                    "success": False,
                    "error": {
                        "code": ResponseStatus.INVALID_REQUEST,
                        "message": "Invalid JSON request body",
                    }
                }
        data['access_token'] = uni(req.headers.get("X-Access-Token", ""))
        kwargs['data'] = data
        return handler(*args, **kwargs)
    return extracted


def wrapJSON(handler):
    def jsoned(*args, **kwargs):
        data = handler(*args, **kwargs)
        r = json.dumps(data)
        return Response(r, content_type='application/json; charset=utf-8')
    return jsoned


def handleError(handler):
    def handled(*args, **kwargs):
        try:
            return handler(*args, **kwargs)
        except ApiError as e:
            resp = {
                "success": False,
                "error": {
                    "code": e.status,
                    "message": e.message
                }
            }
            return resp
    return handled


def validate(validations):
    def decorate(handler):
        def validated(*args, **kwargs):
            for field, functions in validations.iteritems():
                value = kwargs['data'].get(field, None)
                if value is None and validators.required in functions:
                    raise ApiError('Missing required field: %s' % field)

                if value is None and validators.required not in functions:
                    continue

                for function in functions:
                    if function in [validators.required]:
                        continue
                    # extra is either an error message or a processed value
                    success, extra = function(value)
                    if success:
                        kwargs['data'][field] = extra
                    else:
                        raise ApiError('Invalid input for %s: %s' % (field, extra))

            return handler(*args, **kwargs)
        return validated
    return decorate


def api(**kwargs):
    needAuth = kwargs.get("auth", True)
    validations = kwargs.get("validations", {})

    def decorate(handler):
        middlewares = [
            # In
            validate(validations),
            extractData,
            handleError,
            # Out
            wrapJSON
        ]
        if needAuth:
            middlewares.insert(0, doAuth)

        wrapped = handler
        for m in middlewares:
            wrapped = m(wrapped)

        return wrapped
    return decorate
