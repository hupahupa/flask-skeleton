#!/usr/bin/env python

from upaty import *
from upaty.models.models import *
import os


os.environ['PYTHONINSPECT'] = 'True'

ctx = app.test_request_context()
ctx.push()