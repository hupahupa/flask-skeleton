default[:upaty][:app_name] = 'upaty'
default[:upaty][:app_user] = 'upaty'
default[:upaty][:db][:database] = 'upaty'
default[:upaty][:db][:host] = 'localhost'
default[:upaty][:db][:user] = 'upaty'
default[:upaty][:port] = 8125
default[:upaty][:secret_key] = 'dHKEf1IXUV6pHULHaAOUaL0podQzs9ubg'
default[:upaty][:python][:virtualenv] = '/home/upaty/.virtualenvs/upaty'
default[:upaty][:server_aliases] = []

default[:nvm][:reference] = 'v0.5.1'
# This is more like a constant. Don't change it, as v0.11.12 has error
# when run under upstart (https://github.com/joyent/libuv/issues/1207)
default[:upaty][:nodejs][:version] = 'v0.11.11'

#environment: local or dev or prod
default[:upaty][:env] = 'dev'

#emails
default[:upaty][:emails][:admin] = 'duyleminh1402+admin@gmail.com'

default[:upaty][:remote_console] = {
    # :port => 7777,
    # :passwd_file => "passwd"
}

#email to send error
default[:upaty][:emails][:errors] = [
    # 'hung.nguyen@upaty.com',
    # 'tuang.phung@upaty.com',
    # 'duy.le@upaty.com'
]

default[:upaty][:debug] = true

default[:upaty][:apns] = {
    :sandbox => true,
    # :cert_file => "config/apns.pem"
}

default[:upaty][:gcm] = {
    :api_key => "<an-api-key>"
}
